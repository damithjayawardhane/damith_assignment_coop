--------------------------------------------------------
--  File created - Sunday-December-30-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure BANAK_USR_GET_ID
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_DB"."BANAK_USR_GET_ID" 

(
Id_ IN INT,
US_DETAILS_CURSOR OUT SYS_REFCURSOR
)
AS 
BEGIN
  OPEN US_DETAILS_CURSOR FOR
    SELECT ID,NAME,ROLE,PASSWORD,USERNAME FROM COOP_DB.b_user WHERE ID=Id_;
END BANAK_USR_GET_ID;

/
--------------------------------------------------------
--  DDL for Procedure BANK_ACC_ADD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_DB"."BANK_ACC_ADD" 
(
Id_ IN VARCHAR2,
Name_ IN VARCHAR2,
Balance_ IN NUMBER,
Branch_ IN VARCHAR2,
Owner_ IN VARCHAR2
)
AS
BEGIN
INSERT INTO COOP_DB.b_account(Id,Name,Balance,Branch,Owner) VALUES(Id_,Name_,Balance_,Branch_,Owner_);
END BANK_ACC_ADD;

/
--------------------------------------------------------
--  DDL for Procedure BANK_ACC_DELETE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_DB"."BANK_ACC_DELETE" 
(
Id_ IN NUMBER
)
AS 
BEGIN

DELETE FROM COOP_DB.b_account WHERE ID=Id_;
END BANK_ACC_DELETE;

/
--------------------------------------------------------
--  DDL for Procedure BANK_ACC_GET_ALL
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_DB"."BANK_ACC_GET_ALL" 
(
ACCCURSOR OUT SYS_REFCURSOR
)
AS 
BEGIN
  OPEN ACCCURSOR FOR
  SELECT Id,Name,Balance,Branch,Owner FROM COOP_DB.b_account;
END BANK_ACC_GET_ALL;

/
--------------------------------------------------------
--  DDL for Procedure BANK_ACC_GET_ID
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_DB"."BANK_ACC_GET_ID" 

(
Id_ IN INT,
US_DETAILS_CURSOR OUT SYS_REFCURSOR
)
AS 

BEGIN
    OPEN US_DETAILS_CURSOR FOR
    SELECT ID,Name,Balance,Branch,Owner FROM COOP_DB.b_account WHERE ID=Id_;
END BANK_ACC_GET_ID;

/
--------------------------------------------------------
--  DDL for Procedure BANK_ACC_UPDATE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_DB"."BANK_ACC_UPDATE" (
Id_ IN VARCHAR2,
Name_ IN VARCHAR2,
Balance_ IN NUMBER,
Branch_ IN VARCHAR2,
Owner_ IN VARCHAR2
) AS
BEGIN
    UPDATE COOP_DB.b_account
    SET
        
        Name = Name_,
        Balance = Balance_,
        Branch = Branch_,
        Owner = Owner_
    WHERE
        Id = Id_;

END bank_acc_update;

/
--------------------------------------------------------
--  DDL for Procedure BANK_LOGIN
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_DB"."BANK_LOGIN" 

(
USERNAME_ IN VARCHAR2,
PASSWORD_ IN VARCHAR2,
USER_DETAIL_CURSOR OUT SYS_REFCURSOR
)
AS 
BEGIN
 OPEN USER_DETAIL_CURSOR for
 SELECT ID,NAME,ROLE,username FROM COOP_DB.b_user WHERE USERNAME=USERNAME_ AND PASSWORD=PASSWORD_;
END BANK_LOGIN;

/
--------------------------------------------------------
--  DDL for Procedure BANK_USER_DELETE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_DB"."BANK_USER_DELETE" 
(
Id_ IN NUMBER
)
AS 
BEGIN
  DELETE FROM COOP_DB.b_user WHERE ID=Id_;
END BANK_USER_DELETE;

/
--------------------------------------------------------
--  DDL for Procedure BANK_USR_ADD
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_DB"."BANK_USR_ADD" 
(
ID_ IN VARCHAR2,
NAME_ IN VARCHAR2,
ROLE_ IN VARCHAR2,
PASSWORD_ IN VARCHAR2,
USERNAME_ IN VARCHAR2
)
AS 
BEGIN

INSERT INTO COOP_DB.b_user(ID,NAME,ROLE,PASSWORD,USERNAME) VALUES(ID_,NAME_,ROLE_,PASSWORD_,USERNAME_);
  
END BANK_USR_ADD;

/
--------------------------------------------------------
--  DDL for Procedure BANK_USR_GET_ALL
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_DB"."BANK_USR_GET_ALL" 
(
USRCURSOR OUT SYS_REFCURSOR
)
AS 
BEGIN
  OPEN USRCURSOR FOR
  SELECT ID,NAME,ROLE,PASSWORD,USERNAME FROM COOP_DB.b_user;
END BANK_USR_GET_ALL;

/
--------------------------------------------------------
--  DDL for Procedure BANK_USR_UPDATE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_DB"."BANK_USR_UPDATE" 
(
ID_ IN VARCHAR2,
NAME_ IN VARCHAR2,
ROLE_ IN VARCHAR2,
PASSWORD_ IN VARCHAR2,
USERNAME_ IN VARCHAR2
)
AS 
BEGIN
  UPDATE COOP_DB.b_user
    SET
        
        NAME = NAME_,
        ROLE = ROLE_,
        PASSWORD = PASSWORD_,
        USERNAME = USERNAME_
    WHERE
        ID = ID_;
END BANK_USR_UPDATE;

/
