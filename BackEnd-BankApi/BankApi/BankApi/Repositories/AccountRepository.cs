﻿using BankApi.IRepositories;
using BankApi.Model;
using BankApi.Oracle;
using Dapper;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace BankApi.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private IConfiguration configuration;
        public AccountRepository(IConfiguration _configuration)
        {
            configuration = _configuration;
        }
        //Add new account
        public async Task<bool> AddAccount(Account account)
        {
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("Id_", OracleDbType.Int32, ParameterDirection.Input, account.Id);
                dyParam.Add("Name_", OracleDbType.Varchar2, ParameterDirection.Input, account.Name);
                dyParam.Add("Balance_", OracleDbType.Varchar2, ParameterDirection.Input, account.Balance);
                dyParam.Add("Branch_", OracleDbType.Varchar2, ParameterDirection.Input, account.Branch);
                dyParam.Add("Owner_", OracleDbType.Varchar2, ParameterDirection.Input, account.Owner);

                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "BANK_ACC_ADD";
                    await SqlMapper.QueryAsync<Account>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        //Delete Account
        public async Task<bool> DeleteAccount(string Id)
        {
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("Id_", OracleDbType.Int32, ParameterDirection.Input, Id);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "BANK_ACC_DELETE";
                    (await SqlMapper.QueryAsync(conn, query, param: dyParam, commandType: CommandType.StoredProcedure))?.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        //Get account by id
        public async Task<Account> GetAccountById(string Id)
        {
            Account result = null;
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("Id_", OracleDbType.Int32, ParameterDirection.Input, Id);
                dyParam.Add("US_DETAILS_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "BANK_ACC_GET_ID";
                    result = (await SqlMapper.QueryAsync<Account>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure))?.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
        //Get all account
        public async Task<List<Account>> GetAccounts()
        {
            List<Account> result = null;
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("ACCCURSOR", OracleDbType.RefCursor, ParameterDirection.Output);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "BANK_ACC_GET_ALL";
                    result = (await SqlMapper.QueryAsync<Account>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure)).ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
        //Update account
        public async Task<bool> UpdateAccount(Account account)
        {
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("Id_", OracleDbType.Varchar2, ParameterDirection.Input, account.Id);
                dyParam.Add("Name_", OracleDbType.Varchar2, ParameterDirection.Input, account.Name);
                dyParam.Add("Balance_", OracleDbType.Varchar2, ParameterDirection.Input, account.Balance);
                dyParam.Add("Branch_", OracleDbType.Varchar2, ParameterDirection.Input, account.Branch);
                dyParam.Add("Owner_", OracleDbType.Varchar2, ParameterDirection.Input, account.Owner);
                var conn = GetConnection();
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                if (conn.State == ConnectionState.Open)
                {
                    var query = "bank_acc_update";
                    await SqlMapper.QueryAsync<Account>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }


        public IDbConnection GetConnection()
        {
            var connectionString = configuration.GetSection("ConnectionStrings").GetSection("AccountConnection").Value;
            var conn = new OracleConnection(connectionString);
            return conn;
        }
    }
}
