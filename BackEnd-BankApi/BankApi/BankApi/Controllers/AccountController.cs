﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BankApi.IRepositories;
using BankApi.IServices;
using BankApi.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BankApi.Controllers
{  //[Authorize]
    [Route("api/accounts")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        //get All account
        [HttpGet]
        public async Task<IActionResult> GetAccount()
        {
            var acc = await _accountService.GetAccounts();
            return Ok(acc);
        }
        //get acount by Id
        [Route("{Id}")]
        [HttpGet]
        public async Task<IActionResult> GetAccountById(string Id)
        {
            var Acc = await _accountService.GetAccountById(Id);
            if (Acc != null)
            {
                return Ok(Acc);
            }
            else
            {
                return NotFound("Account not found with given id");
            }
        }

        //Add new account
        [HttpPost]
        public async Task<IActionResult> AddAccount([FromBody]Account account)
        {
            var newProduct = await _accountService.AddAccount(account);
            return Ok(newProduct);
        }
        //[Authorize(Policy = "AdminOnly")]
        //Delete account
        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteAccount(string Id)
        {
            var accDeleted = await _accountService.DeleteAccount(Id);
            if (accDeleted == false)
            {
                return NotFound("No account found with given id");
            }
            return Ok(accDeleted);
        }
        //update account
        [HttpPut]
        public async Task<IActionResult> UpdateAccount([FromBody]Account account)
        {
            var updatedaccount = await _accountService.UpdateAccount(account);
            if (updatedaccount == false)
            {
                return NotFound("No account found with given id");
            }
            return Ok(updatedaccount);
        }

    }
}